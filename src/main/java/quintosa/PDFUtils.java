package quintosa;

import io.woo.htmltopdf.HtmlToPdf;
import io.woo.htmltopdf.HtmlToPdfObject;
import io.woo.htmltopdf.PdfPageSize;

import java.io.InputStream;

/**
 * The type Pdf utils.
 * <p>
 * Cualquier problema con el JAR revisar este enlace:
 * https://github.com/wooio/htmltopdf-java
 * <p>
 * O este:
 * https://jira.favorita.ec/browse/PTH-2277
 *
 * @author mquintosa
 */
final class PDFUtils {

    /**
     *
     */
    private PDFUtils() {
        // constructor stub
    }

    /**
     * Convert html to pdf input stream.
     *
     * @param html  the html
     * @param title the title
     * @return the input stream
     */
    static InputStream convertHtmlToPdf(String html, String title) {
        return HtmlToPdf.create().documentTitle(title)

                // class = text-huge - LETRA GRANDE SE VE MAL CON MAS DE 45
                // LETRA NORMAL SE VE MAL CON MAS DE 86
                .dpi(45)
                .pageSize(PdfPageSize.A4)
                .object(HtmlToPdfObject.forHtml(html))
                .convert();
    }
}
